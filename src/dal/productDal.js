require('dotenv').config({ path: './variables.env' });
const connectToDatabase = require('../../utils/db');
const Helper = require('../../utils/helper');
const productData = require('../models/productData.model.js');


module.exports = class Product{
	constructor(){

	}
	addProductToDB(productItem){
		// TODO: if product Action: Deactivated then Operation Type is delete
		console.log("productItem >>", productItem);
		return connectToDatabase().then(() =>{
		    console.log("inside then", Object.keys(productItem));
		    console.log("pageActionType", productItem['pageActionType'].toLowerCase());
		    if(productItem.hasOwnProperty('pageActionType') 
		    	&&  ['delete', 'deactivate'].indexOf(productItem['pageActionType'].toLowerCase())>-1
		    	&& productItem['operationType'].toLowerCase() === 'update'){
		    	console.log("inside if then");
		    	productItem['operationType'] = 'Delete'
		    }
		    return productData.create(productItem)
		      .then(product =>{
		        console.log("inside db insertion", product);
		        return {
		          statusCode: 200,
		          message:"success",
		          errorDetails: {
		          	"errorMessage": null
		          }
		        }
		      })
		      .catch(err =>{
		        return  {
		          statusCode: err.statusCode || 500,
		          message:"failure",
		          error: err,
		          errorDetails: {
		          	"errorMessage": 'Could not create the products'
		          }
		        }
		      })

		})
	}

	getAdaptiveJson(batchId, locale, brand){
		return connectToDatabase().then(() =>{
			return productData.find({
				'batchId': batchId
			}).select(' -_id').lean()
			.then((productItems)=>{
				let helper = new Helper();
				return helper.createAdaptiveJson(productItems)
				.then((adaptiveJson)=>{
					let data = {
						"batchId": batchId,
						"brand": brand,
						"locale": locale,
						"productData": adaptiveJson.productData
					};
					return data;
				})

			})
			.catch((err)=>{
				return {
					statusCode: err.statusCode || 500,
					message:"failure",
					error: err,
					errorDetails: {
						"errorMessage": 'Could not connect to the database.'
					}
		        }
			})
		
		})
	}
}  
