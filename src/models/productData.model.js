// product.model.js
const mongoose = require('mongoose'),
	Schema = mongoose.Schema;
const ProductDataSchema = new mongoose.Schema({
  locale: {
    type: String
  },
  brand: {
    type: String
  },
  operationType:{
  	type: String,
	enum: ['Create', 'Update', 'Delete']
  },
  binProvider:{
  	type: String
  },
  productdata:{
  	type: Schema.Types.Mixed
  },
  environment: {
  	type: String,
  	enum: ['nonprod', 'prod']
  },
  environmentUrl: {
  	type: String
  },
  status: {
	type: String,
	enum: ['unprocessed', 'inprogress', 'processed'],
	default: 'unprocessed'
  },
  batchId: {
  	type: String,
  	unique : true
  }
}, { strict: false });
module.exports = mongoose.model('ProductData', ProductDataSchema);