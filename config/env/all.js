/**
 * Common config for all environments
 * Overwrite for specific env in that env's file
 */

module.exports = {
    app: {
        title: 'O2O',
        description: 'O2O description',
        keywords: 'O2O keywrods'
    },
    port: process.env.PORT || 3000,
    binProvider:{
        prod: {
            cartwire: {
                url:"http://cwqa.srmtechsol.com/SNWLive/Aem_postrequest/update_ean",
                client_key: "QmWnEbRvTcYxUz753869421",
                api_key: "h0Tg439WGl0/ML2iufwIDx2TdKmvP1ShRpRKodsWmiQ=",
                template: {
                    "operationType":"operationType",
                    "locale": "locale",
                    "brand": "brand",
                    "category": "category",
                    "subCategory": "sub_category",
                    "productTitle": "product_title",
                    "productUrl": "product_url",
                    "imageUrl": "image_url",
                    "description": "description",
                    "productSize": "product_size",
                    "countryName": "country_name",
                    "eanNumber": "ean_number"
                }
            }
        },
        nonprod: {
            cartwire: {
                url:"http://cwqa.srmtechsol.com/SNWLive/Aem_postrequest/update_ean",
                client_key: "QmWnEbRvTcYxUz753869421",
                api_key: "h0Tg439WGl0/ML2iufwIDx2TdKmvP1ShRpRKodsWmiQ=",
                template: {
                    "operationType":"operationType",
                    "locale": "locale",
                    "brand": "brand",
                    "category": "category",
                    "subCategory": "sub_category",
                    "productTitle": "product_title",
                    "productUrl": "product_url",
                    "imageUrl": "image_url",
                    "description": "description",
                    "productSize": "product_size",
                    "countryName": "country_name",
                    "eanNumber": "ean_number"
                }
            }
        }
        
    }
};