const mongoose = require('mongoose');
const config = require('../config');
mongoose.Promise = global.Promise;
let isConnected;
module.exports = connectToDatabase = () => {
  if (isConnected) {
    console.log('=> using existing database connection');
    return Promise.resolve();
  }
  console.log('=> using new database connection');
  return mongoose.connect(process.env.DB || config.mongouri).then(db => {
    isConnected = db.connections[0].readyState;
  });
};