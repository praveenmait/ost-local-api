const transform = require('jsonpath-object-transform');
const fs = require('fs');
const _ = require('lodash');
const config = require('../config');

module.exports = class Helper{
	constructor(){

	}
	async createAdaptiveJson(data){
		let binProvider = data[0]['binProvider'];
		let binConfig =await this.readConfig(binProvider);
		let transformedData = await this.jsonTransformation(binConfig, data);
		console.log("transformedData :>>>>", transformedData);
		return transformedData;

	}


	readConfig(binProvider) {
		console.log("binProvider >>", binProvider.toLowerCase());
		return new Promise((resolve, reject) => {
			const path = './binProviderConfig/' + binProvider.toLowerCase() + '.json'
			fs.access(path, fs.F_OK, (err) => {
				if (err) {
					console.error("bin Provider config file not present", err)
					return  {
						statusCode: err.statusCode || 500,
						message:"failure",
						errorDetails: {
							"errorMessage": 'Could not find '+binProvider+' file.'
						}
			        }
				}
				let data = fs.readFileSync(path, 'utf8');
				binProvider = JSON.parse(data);
				resolve(binProvider);
			})
		})
	}

	jsonTransformation(config, data) {
		console.log("jsonTransformation >>", data);
		let productBatchData = {};
		productBatchData['data']=data;
		let result = transform(productBatchData, config.template);
		console.log("result :>>>>", result);
		return result;
		
	}


}


// function tranformToBinProviderTemplate(binProvider, product, productRespJson){
// 		let type = config.environment.type;
// 		console.log("type >>", type);
// 		let template = config['binProvider'][type][binProvider]['template'];
// 		console.log("template >>>", template);
// 		console.log("product >>>", product);
// 		console.log("product >>>", Object.keys(product));
// 		_.forEach(Object.keys(product), function(productKey){
// 			console.log("inside if ???????????", productKey);
// 			// console.log("inside if ???????????", value);
// 			if(typeof product[productKey] === 'object'){
// 				console.log("inside if ???????????");
// 				tranformToBinProviderTemplate(binProvider, product[productKey], productRespJson)
// 			}
// 			var tempKey;
// 			if(template.hasOwnProperty(productKey)){
// 				tempKey = template['productKey']	
// 			}else {
// 				tempKey = productKey
// 			}
// 			productRespJson[tempKey] = product[productKey]
// 		})
// 		return productRespJson

// 	}