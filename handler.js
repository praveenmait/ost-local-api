'use strict';
const Product = require('./src/dal/productDal');

module.exports.hello = async event => {
  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: 'Go Serverless v1.0! Your function executed successfully!',
        input: event,
      },
      null,
      2
    ),
  };

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};

module.exports.create = async event => {
  // context.callbackWaitsForEmptyEventLoop = false
  console.log("event:", event);
  let productData = new Product();
  return productData.addProductToDB(event);
}

module.exports.getAdaptiveJson = async event =>{
  let batchId, binProvider, locale, brand;
  console.log("event:", event);
  if(event.hasOwnProperty('batchId')){
    batchId = event.batchId;
  }
  if(event.hasOwnProperty('locale')){
    locale = event.locale;
  }
  if(event.hasOwnProperty('brand')){
    brand = event.brand;
  }
  let productData = new Product();
  return productData.getAdaptiveJson(batchId, locale, brand)
}